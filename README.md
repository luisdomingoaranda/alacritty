# [https://github.com/alacritty/alacritty](Alacritty dotfiles)

GPU-accelerated terminal emulator. 

You can get a look at a blank config file for alacritty [here](https://github.com/alacritty/alacritty/blob/master/alacritty.yml).

## Vim-mode

`Ctrl+Shift+Space`

To toggle vim-mode which allows you to scroll back, yank and search using vim
keybindings


## Fonts
* `fc-list : family style` to show what families and styles you can setup in your config file
* `fc-list -f '%{file}\n' | sort` show all the fonts installed in your system
* `fc-match "Anonymice Nerd Font Bold"` show which font would be selected given that description

Or use a font previewer, such as "font-manager".  


## [Color themes](https://github.com/eendroroy/alacritty-theme)
Alacritty doesn't provide a way to read from `.Xresource` to get color values.

Under folder themes you can find a variety of color schemes used by alacritty.  

You can easily switch between color schemes, by setting the colors options, after defining all possible schemes in `alacirtty.yml`.  
```
colors: *ayu_dark
```
